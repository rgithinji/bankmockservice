// @GENERATOR:play-routes-compiler
// @SOURCE:C:/SBT_Workspace/bankaccountservice_0/conf/routes
// @DATE:Fri Jul 27 14:43:01 EAT 2018

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseRequestController RequestController = new controllers.ReverseRequestController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseRequestController RequestController = new controllers.javascript.ReverseRequestController(RoutesPrefix.byNamePrefix());
  }

}
