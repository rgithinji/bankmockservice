// @GENERATOR:play-routes-compiler
// @SOURCE:C:/SBT_Workspace/bankaccountservice_0/conf/routes
// @DATE:Fri Jul 27 14:43:01 EAT 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers {

  // @LINE:12
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }

  // @LINE:7
  class ReverseRequestController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def insertTestRecord(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "xyzbank/createTestRecord")
    }
  
    // @LINE:9
    def withdrawPut(): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "xyzbank/account/withdraw/withdrawPut")
    }
  
    // @LINE:10
    def debug(): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "xyzbank/debug")
    }
  
    // @LINE:7
    def getBalance(account_id:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "xyzbank/account/balance/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("account_id", account_id)))
    }
  
    // @LINE:13
    def catchall(path:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + implicitly[play.api.mvc.PathBindable[String]].unbind("path", path))
    }
  
    // @LINE:8
    def depositPut(): Call = {
      
      Call("PUT", _prefix + { _defaultPrefix } + "xyzbank/account/deposit/depositPut")
    }
  
  }


}
