// @GENERATOR:play-routes-compiler
// @SOURCE:C:/SBT_Workspace/bankaccountservice_0/conf/routes
// @DATE:Fri Jul 27 14:43:01 EAT 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset

// @LINE:7
package controllers.javascript {

  // @LINE:12
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:7
  class ReverseRequestController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def insertTestRecord: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RequestController.insertTestRecord",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "xyzbank/createTestRecord"})
        }
      """
    )
  
    // @LINE:9
    def withdrawPut: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RequestController.withdrawPut",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "xyzbank/account/withdraw/withdrawPut"})
        }
      """
    )
  
    // @LINE:10
    def debug: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RequestController.debug",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "xyzbank/debug"})
        }
      """
    )
  
    // @LINE:7
    def getBalance: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RequestController.getBalance",
      """
        function(account_id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "xyzbank/account/balance/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("account_id", account_id0))})
        }
      """
    )
  
    // @LINE:13
    def catchall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RequestController.catchall",
      """
        function(path0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("path", path0)})
        }
      """
    )
  
    // @LINE:8
    def depositPut: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.RequestController.depositPut",
      """
        function() {
          return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "xyzbank/account/deposit/depositPut"})
        }
      """
    )
  
  }


}
