// @GENERATOR:play-routes-compiler
// @SOURCE:C:/SBT_Workspace/bankaccountservice_0/conf/routes
// @DATE:Fri Jul 27 14:43:01 EAT 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:7
  RequestController_1: controllers.RequestController,
  // @LINE:12
  Assets_0: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:7
    RequestController_1: controllers.RequestController,
    // @LINE:12
    Assets_0: controllers.Assets
  ) = this(errorHandler, RequestController_1, Assets_0, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, RequestController_1, Assets_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """xyzbank/account/balance/""" + "$" + """account_id<[^/]+>""", """controllers.RequestController.getBalance(account_id:String)"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """xyzbank/account/deposit/depositPut""", """controllers.RequestController.depositPut"""),
    ("""PUT""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """xyzbank/account/withdraw/withdrawPut""", """controllers.RequestController.withdrawPut"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """xyzbank/debug""", """controllers.RequestController.debug"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """xyzbank/createTestRecord""", """controllers.RequestController.insertTestRecord"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """path<.*>""", """controllers.RequestController.catchall(path:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:7
  private[this] lazy val controllers_RequestController_getBalance0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("xyzbank/account/balance/"), DynamicPart("account_id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_RequestController_getBalance0_invoker = createInvoker(
    RequestController_1.getBalance(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RequestController",
      "getBalance",
      Seq(classOf[String]),
      "GET",
      this.prefix + """xyzbank/account/balance/""" + "$" + """account_id<[^/]+>""",
      """ An example controller showing a sample home page""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_RequestController_depositPut1_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("xyzbank/account/deposit/depositPut")))
  )
  private[this] lazy val controllers_RequestController_depositPut1_invoker = createInvoker(
    RequestController_1.depositPut,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RequestController",
      "depositPut",
      Nil,
      "PUT",
      this.prefix + """xyzbank/account/deposit/depositPut""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_RequestController_withdrawPut2_route = Route("PUT",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("xyzbank/account/withdraw/withdrawPut")))
  )
  private[this] lazy val controllers_RequestController_withdrawPut2_invoker = createInvoker(
    RequestController_1.withdrawPut,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RequestController",
      "withdrawPut",
      Nil,
      "PUT",
      this.prefix + """xyzbank/account/withdraw/withdrawPut""",
      """""",
      Seq()
    )
  )

  // @LINE:10
  private[this] lazy val controllers_RequestController_debug3_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("xyzbank/debug")))
  )
  private[this] lazy val controllers_RequestController_debug3_invoker = createInvoker(
    RequestController_1.debug,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RequestController",
      "debug",
      Nil,
      "GET",
      this.prefix + """xyzbank/debug""",
      """""",
      Seq()
    )
  )

  // @LINE:11
  private[this] lazy val controllers_RequestController_insertTestRecord4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("xyzbank/createTestRecord")))
  )
  private[this] lazy val controllers_RequestController_insertTestRecord4_invoker = createInvoker(
    RequestController_1.insertTestRecord,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RequestController",
      "insertTestRecord",
      Nil,
      "GET",
      this.prefix + """xyzbank/createTestRecord""",
      """""",
      Seq()
    )
  )

  // @LINE:12
  private[this] lazy val controllers_Assets_versioned5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned5_invoker = createInvoker(
    Assets_0.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_RequestController_catchall6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("path", """.*""",false)))
  )
  private[this] lazy val controllers_RequestController_catchall6_invoker = createInvoker(
    RequestController_1.catchall(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RequestController",
      "catchall",
      Seq(classOf[String]),
      "GET",
      this.prefix + """""" + "$" + """path<.*>""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:7
    case controllers_RequestController_getBalance0_route(params@_) =>
      call(params.fromPath[String]("account_id", None)) { (account_id) =>
        controllers_RequestController_getBalance0_invoker.call(RequestController_1.getBalance(account_id))
      }
  
    // @LINE:8
    case controllers_RequestController_depositPut1_route(params@_) =>
      call { 
        controllers_RequestController_depositPut1_invoker.call(RequestController_1.depositPut)
      }
  
    // @LINE:9
    case controllers_RequestController_withdrawPut2_route(params@_) =>
      call { 
        controllers_RequestController_withdrawPut2_invoker.call(RequestController_1.withdrawPut)
      }
  
    // @LINE:10
    case controllers_RequestController_debug3_route(params@_) =>
      call { 
        controllers_RequestController_debug3_invoker.call(RequestController_1.debug)
      }
  
    // @LINE:11
    case controllers_RequestController_insertTestRecord4_route(params@_) =>
      call { 
        controllers_RequestController_insertTestRecord4_invoker.call(RequestController_1.insertTestRecord)
      }
  
    // @LINE:12
    case controllers_Assets_versioned5_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned5_invoker.call(Assets_0.versioned(path, file))
      }
  
    // @LINE:13
    case controllers_RequestController_catchall6_route(params@_) =>
      call(params.fromPath[String]("path", None)) { (path) =>
        controllers_RequestController_catchall6_invoker.call(RequestController_1.catchall(path))
      }
  }
}
