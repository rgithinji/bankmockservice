// @GENERATOR:play-routes-compiler
// @SOURCE:C:/SBT_Workspace/bankaccountservice_0/conf/routes
// @DATE:Fri Jul 27 14:43:01 EAT 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
